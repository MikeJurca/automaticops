﻿using System;

namespace UnityBuildingApp
{
    public class Program
    {

        public static void Main(string[] args)
        {
            int sleep = 1000;

            Downloader downloader = new Downloader(sleep);

            downloader.NavigateToPage("https://developer.cloud.unity3d.com/projects/");
            //Wait for website to fully load
            Wait(sleep);

            downloader.Login();
            Wait(sleep);

            downloader.NavigateToPage("https://developer.cloud.unity3d.com/build/orgs/geosteeringsolutionsinc/projects/geodirect/");
            Wait(sleep*3);

            downloader.DownloadLatest("Default Windows desktop 64-bit");
            //downloader.DownloadLatest("Default Mac desktop Universal");
            downloader.DownloadLatest("Default Android");
            Wait(sleep * 10);
            downloader.Quit();
        }

        public static void Wait(int sleep)
        {
            System.Threading.Thread.Sleep(sleep);
        }
    }
}
