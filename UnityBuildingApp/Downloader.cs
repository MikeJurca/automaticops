﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace UnityBuildingApp
{
    public class Downloader
    {
        private bool validURL;

        int sleep;

        ChromeDriver driver;
      

        public Downloader(int sleepNum)
        {
            sleep = sleepNum;
            SetUpChromeBrowser();
        }

        public void SetUpChromeBrowser()
        {

            var chromeOptions = new ChromeOptions();
            chromeOptions.AddArguments("--disable-notifications");
            chromeOptions.AddArguments("--start-maximized");
            chromeOptions.AddArguments("disable-infobars");
            chromeOptions.AddArguments("--disable-gpu");
            chromeOptions.AddArguments("--headless");
            chromeOptions.AddArguments("window-size=1980,1080");
            chromeOptions.AddArguments("--allow-running-insecure-content");
            chromeOptions.AddArguments("--disable-extensions");
            chromeOptions.AddArguments("--no-sandbox");
            chromeOptions.AddArguments("--ignore-certificate-errors");


            
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), chromeOptions);
            

            Dictionary<string, object> commandParameters = new Dictionary<string, object>
            {
                ["behavior"] = "allow",
                ["downloadPath"] = @"C:\Downloads"
            };

            driver.ExecuteChromeCommand("Page.setDownloadBehavior", commandParameters);
        }



        public void NavigateToPage(string WebAddress)
        {
            try
            {
                driver.Navigate().GoToUrl(WebAddress);
                validURL = true;
            }
            catch (Exception ex)
            {
                validURL = false;
            }



        }

        public void Login()
        {
            var username = driver.FindElementById("conversations_create_session_form_email");
            username.Clear();
            username.SendKeys("mjurca@geo-steer.com");


            var password = driver.FindElementById("conversations_create_session_form_password");
            password.Clear();
            password.SendKeys("Roadster1000!");

            driver.FindElementByName("commit").Click();

        }

        public void DownloadLatest(string findText)
        {
            var buildHistoryElements = driver.FindElementsByTagName("build-card");

            List<IWebElement> windowsBuildCards = new List<IWebElement>();

            foreach (var buildCard in buildHistoryElements)
            {
                if (buildCard.FindElement(By.ClassName("targetname")).Text.Contains(findText))
                {
                    windowsBuildCards.Add(buildCard);
                }
            }

            List<IWebElement> successCards = new List<IWebElement>();

            
            foreach (var windowsCard in windowsBuildCards)
            {
                try
                {
                    if (windowsCard.FindElement(By.ClassName("buildStatus")).Text.Contains("Success"))
                    {
                        successCards.Add(windowsCard);
                    }
                }
                catch { }
            }

            if (successCards.Count > 0)
            {
               
                IWebElement recentCard = successCards.First();
                recentCard.Click();



            }
          
            var buildHistoryElement = driver.FindElementByTagName("build-sidebar");
            
            buildHistoryElement.FindElement(By.ClassName("md-ink-ripple")).Click();

        }

        public void Quit()
        {
            driver.Quit();
        }
    }
}
